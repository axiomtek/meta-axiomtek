DESCRIPTION = "Microchip AR1XXX Serial Touchscreen"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://license.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRC_URI = "file://AR1010-AR1011-AR1100-LINUX-UART-V102.tar.gz \
	   file://touchstart \
"

S = "${WORKDIR}/AR1010-AR1011-AR1100-LINUX-UART-V102"

DEPENDS_append = " update-rc.d-native"
#INSANE_SKIP_${PN} = "ldflags already-stripped"

do_configure() {
}

do_compile() {
	${CC} ${CFLAGS} ${LDFLAGS} inputactivate.c -o inputactivate
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${S}/inputactivate ${D}${bindir}/

	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${S}/10-ar1xxxuart.rules ${D}${sysconfdir}/udev/rules.d/

	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${sysconfdir}/rc2.d
	install -d ${D}${sysconfdir}/rc3.d
	install -d ${D}${sysconfdir}/rc4.d
	install -d ${D}${sysconfdir}/rc5.d

	install -m 0755 ${WORKDIR}/touchstart ${D}${sysconfdir}/init.d/
#
# Create runlevel links
#
	update-rc.d -r ${D} touchstart start 20 2 3 4 5 .
}
