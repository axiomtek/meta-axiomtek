FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
DEPENDS_append = " update-rc.d-native"

SRC_URI += " \
	file://pt-udev-rules \
	file://pt-udev-rules.service \
	file://calibrate_table \
"

BACKEND = \
    "${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'wayland', \
        bb.utils.contains('DISTRO_FEATURES',     'x11',     'x11', \
                                                             'fb', d), d)}"

do_install_append_ptseries() {
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${systemd_unitdir}/system/sysinit.target.wants/
	
	install -m 0755 ${S}/pt-udev-rules ${D}${sysconfdir}/init.d/
	install -m 0644 ${S}/pt-udev-rules.service ${D}${systemd_unitdir}/system

	# Enable pt-udev-rules service
	ln -sf ${systemd_unitdir}/system/pt-udev-rules.service \
		${D}${systemd_unitdir}/system/sysinit.target.wants/pt-udev-rules.service

	if [ "${BACKEND}" = "wayland" ]; then
		install -d ${D}${sysconfdir}/default
		install -m 0644 ${S}/calibrate_table ${D}${sysconfdir}/default/
	fi
}
