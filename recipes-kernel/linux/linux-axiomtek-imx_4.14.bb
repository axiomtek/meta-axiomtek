SUMMARY = "Linux kernel for Axiomtek i.MX platform"

require recipes-kernel/linux/linux-imx.inc

DEPENDS += "lzop-native bc-native"

SRC_URI = " \
	git://gitlab.com/axiomtek/linux-imx.git;protocol=https;branch=${SRCBRANCH} \
	file://defconfig \
	file://0001-uapi-Add-ion.h-to-userspace.patch \
"

LOCALVERSION = "-axl-2.3.0"
SRCBRANCH = "axiomtek-imx_4.14.x-2.3.0"
SRCREV = "30616894fbb97c3e6de0ef65d5d5270ec21903de"
# For developing purpose hide scm version
#SCMVERSION = "n"

addtask copy_defconfig after do_unpack before do_preconfigure
do_copy_defconfig_scm180evk () {
    install -d ${B}
    # copy latest defconfig to use for mx8
    mkdir -p ${B}
    cp ${S}/arch/arm64/configs/scm180_defconfig ${B}/.config
    cp ${S}/arch/arm64/configs/scm180_defconfig ${B}/../defconfig
}


do_copy_defconfig () {
}

COMPATIBLE_MACHINE = "(mx6|mx7|mx8|scm120evk|scm180evk|ptseries|rbox630)"
EXTRA_OEMAKE_append_mx6 = " ARCH=arm"
EXTRA_OEMAKE_append_mx7 = " ARCH=arm"
EXTRA_OEMAKE_append_mx8 = " ARCH=arm64"
