require recipes-bsp/u-boot/u-boot.inc

inherit pythonnative
PROVIDES += "u-boot"
DEPENDS_append = "python dtc-native"

DESCRIPTION = "U-Boot for Axiomtek devices"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"

SRCBRANCH = "axiomtek-v2018.03"
UBOOT_SRC ?= "git://gitlab.com/axiomtek/u-boot.git;protocol=https"
SRC_URI = "${UBOOT_SRC};branch=${SRCBRANCH}"
SRCREV = "4113e354863422a56054a83ecb86f855657ed0bc"
#SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

PV = "v2018.03+git${SRCPV}"

SRC_URI_append_rbox630 = " \
	file://rbox630/0001-rbox630-set-rbox630-revb-as-default-fdt_file-in-.patch \
"

inherit fsl-u-boot-localversion

LOCALVERSION ?= "-axiomtek"

BOOT_TOOLS = "imx-boot-tools"

do_deploy_append_mx8m () {
    # Deploy the mkimage, u-boot-nodtb.bin and scm180.dtb for mkimage to generate boot binary
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    install -d ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/arch/arm/dts/scm180.dtb  ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/tools/mkimage  ${DEPLOYDIR}/${BOOT_TOOLS}/mkimage_uboot
                    install -m 0777 ${B}/${config}/u-boot-nodtb.bin  ${DEPLOYDIR}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${UBOOT_CONFIG}
                fi
            done
            unset  j
        done
        unset  i
    fi

}

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(mx6|mx6ul|mx7|mx8|scm120evk|scm180evk|ptseries|rbox630)"

UBOOT_NAME_mx6 = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
UBOOT_NAME_mx7 = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
UBOOT_NAME_mx8 = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
